import './App.css';
import { Outlet } from 'react-router-dom';
import Navigates from './components/Navigates';

function App() {
  return (
    <div className="App">
      <Navigates></Navigates>
      <Outlet></Outlet>
    </div>
  );
}

export default App;
