import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import NotFoundPage from './pages/NotFoundPage';
import HelpPage from './pages/HelpPage';
import DoneListPage from './pages/DoneListPage';
import { Provider } from 'react-redux';
import store from './app/store';
import TodoList from './components/TodoList';
import DoneDetail from './pages/DoneDetail';



const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: '/',
    element: <App></App>,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: '/help',
        element: <HelpPage></HelpPage>
      },
      {
        path: '/done',
        element: <DoneListPage></DoneListPage>
      },
      {
        path: '/done/:id',
        element: <DoneDetail></DoneDetail>
      }
    ]
  },
  {
    path: '*',
    element: <NotFoundPage></NotFoundPage>
  }
])

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
