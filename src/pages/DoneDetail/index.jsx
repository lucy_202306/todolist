import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Card } from 'antd';
import useTodos from "../../hooks/useTodos";
import "./index.css";

const DoneDetail = () => {
  const params = useParams();
  const id = params.id;
  const [detaile, setDetaile] = useState();
  const todos = useTodos();
  const navigate = useNavigate();

  useEffect(() => {
    (async() => {
      try {
        const data = await todos.getTodoById(id);
        setDetaile(data)
      } catch (error) {
        alert(error.response.data.message)
        navigate("/404");
      }
    })();
  }, [id]);

  return (
    <>
      <Card title="todo detail" bordered hoverable className="todo-detail">
        <p>id: {detaile?.id}</p>
        <p>text: {detaile?.text}</p>
      </Card>
    </>
  );
};

export default DoneDetail;
