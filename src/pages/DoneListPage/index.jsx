import { List } from "antd";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "../../components/TodoItem/index.css";
import useTodos from "../../hooks/useTodos";
import "./index.css";

const DoneListPage = () => {
  const [doneList, setDoneList] = useState([]);
  const todos = useTodos();
  const navigate = useNavigate();
  const getDoneDetail = (id) => {
    navigate(`/done/${id}`);
  };

  const getDoneList = async () => {
    const data = await todos.getTodos();
    const filterData = data.filter(item => item.done);
    setDoneList(filterData);
  }

  useEffect(() => {
    getDoneList();
  }, []);

  return (
    <>
      <List
        className="demo-loadmore-list done-list"
        itemLayout="horizontal"
        dataSource={doneList}
        bordered
        renderItem={(item) => (
          <List.Item
          >
            <div
              className="item-content"
              title={item.text}
              onClick={() => getDoneDetail(item.id)}
            >
              {item.text}
            </div>
          </List.Item>
        )}
      />
    </>
  );
};

export default DoneListPage;
