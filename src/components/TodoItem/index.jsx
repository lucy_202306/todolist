import "./index.css";
import useTodos from "../../hooks/useTodos";
import { Button, List } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useState } from "react";
import TodoItemUpdate from "../TodoItemUpdate";

const TodoItem = (props) => {
  const todos = useTodos();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleChange = (status) => {
    setIsModalOpen(status);
  };

  const handleDelete = (id) => {
    if (window.confirm("Do you want to delete your todo item?")) {
      todos.deleteTodo(id);
    }
  };

  const done = (id, {done, text}) => {
    console.log(id, {done, text})
  }

  return (
    <>
      <List.Item
        actions={ props.showActions ? [
          <Button
            danger
            icon={
              <DeleteOutlined onClick={() => handleDelete(props.value.id)} />
            } 
          ></Button>,
          <Button
            type="primary"
            ghost
            icon={<EditOutlined onClick={() => setIsModalOpen(true)} />}
          ></Button>,
        ]:[]}
      >
        <div
          className={
            props.value.done ? "item-content done-item" : "item-content"
          }
          title={props.value.text}
          onClick={() => todos.updateTodo(props.value.id, {done: !props.value.done})}
        >
          {props.value.text}
        </div>
      </List.Item>
      <TodoItemUpdate
        isModalOpen={isModalOpen}
        onChange={handleChange}
        item={props.value}
      ></TodoItemUpdate>
    </>
  );
};

export default TodoItem;
