import { useSelector } from "react-redux";
import TodoItem from "../TodoItem";
import useTodos from "../../hooks/useTodos";
import React, { useEffect, useState } from "react";
import { Avatar, Button, List, Skeleton } from "antd";
import "./index.css";

const TodoGroup = () => {
  const todos = useTodos();
  const todoList = useSelector((state) => state.todoList.todoList);
  const show = true;

  useEffect(() => {
    todos.getTodos();
  }, []);

  return (
    <>
      <List
        className="demo-loadmore-list todo-list"
        itemLayout="horizontal"
        dataSource={todoList}
        bordered
        renderItem={(item) => (
          <TodoItem key={item.id} value={item} showActions={show}></TodoItem>
        )}
      />
    </>
  );
};

export default TodoGroup;
