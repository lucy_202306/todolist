import React, { useState } from "react";
import { Button, Modal, Input } from "antd";
import useTodos from "../../hooks/useTodos";

const TodoItemUpdate = (props) => {
  const [text, setText] = useState();
  const todos = useTodos();

  const handleOk = async () => {
    await todos.updateTodo(props.item.id, {done: props.item.done, text});
    props.onChange(false);
  };

  const handleCancel = () => {
    props.onChange(false);
  };

  const handleInput = (e) => {
    setText(e.target.value);
  };

  return (
    <>
      <Modal
        title="update todoItem"
        open={props.isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        destroyOnClose
      >
        <Input
          allowClear
          onChange={handleInput}
          defaultValue={props.item.text}
        />
      </Modal>
    </>
  );
};

export default TodoItemUpdate;
