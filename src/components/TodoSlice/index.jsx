import { createSlice } from "@reduxjs/toolkit";

export const todoListSlice = createSlice({
  name: "todoList",
  initialState: {
    todoList: [],
  },
  reducers: {
    loadTodos: (state, action) => {
      state.todoList = action.payload;
    },
  },
});

export const { loadTodos } = todoListSlice.actions;

export default todoListSlice.reducer;
