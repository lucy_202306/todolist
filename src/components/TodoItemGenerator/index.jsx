import { useState } from "react";
import "./index.css";
import useTodos from "../../hooks/useTodos";
import { Button, Input, Space } from "antd";

const TodoItemGenerator = () => {
  const [content, setContent] = useState("");
  const todos = useTodos();

  const handleChangeInput = (e) => {
    setContent(e.target.value);
  };

  const handleChange = async () => {
    if (content != "") {
      await todos.createTodo(content);
      setContent("");
    }
  };

  return (
    <div className="todo-item-generator">
      <Input
        className="add-input"
        value={content}
        onChange={handleChangeInput}
        placeholder="please input your todoItem"
      />
      <Button type="primary" onClick={handleChange}>
        add
      </Button>
    </div>
  );
};

export default TodoItemGenerator;
