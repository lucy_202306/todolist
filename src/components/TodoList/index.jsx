import TodoGroup from "../TodoGroup";
import TodoItemGenerator from "../TodoItemGenerator";
import "./index.css";

const TodoList = () => {
  return (
    <div className="todo-home">
      <h3>Todo List</h3>
      <TodoItemGenerator></TodoItemGenerator>
      <TodoGroup></TodoGroup>
    </div>
  );
};

export default TodoList;
