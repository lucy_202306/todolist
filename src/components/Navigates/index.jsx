import React, { useEffect, useState } from "react";
import { Menu } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
const items = [
  {
    label: "TodoList Page",
    key: "/",
  },
  {
    label: "Done Page",
    key: "/done",
  },
  {
    label: "Help Page",
    key: "/help",
  },
  {
    label: "404 Page",
    key: "/404",
  },
];
const Navigates = () => {
  const [current, setCurrent] = useState("/");
  const navigate = useNavigate();
  const location = useLocation();
  const onClick = (e) => {
    navigate(e.key);
  };

  useEffect(() => {
    setCurrent(location.pathname)
  }, [location.pathname])

  return (
    <Menu
      onClick={onClick}
      selectedKeys={[current]}
      items={items}
    />
  );
};

export default Navigates;
