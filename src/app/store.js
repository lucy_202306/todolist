import { configureStore } from "@reduxjs/toolkit";
import todoListReducer from "../components/TodoSlice/index.jsx";

export default configureStore({
  reducer: {
    todoList: todoListReducer,
  },
});
