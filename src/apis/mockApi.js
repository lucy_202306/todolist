import axios from "axios";

const mockApi = axios.create({
  baseURL: "https://64c0b69b0d8e251fd112660c.mockapi.io/api/",
});

export default mockApi;