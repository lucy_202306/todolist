import axios from "axios";

const Api = axios.create({
  baseURL: "http://localhost:8588",
});

export default Api;