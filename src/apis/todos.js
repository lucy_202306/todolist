import mockApi from "./mockApi";
import Api from "./api";

export const getTodos = async () => {
  return Api.get("/todos");
};

export const getTodoById = async (id) => {
  return Api.get(`/todos/${id}`);
};

export const createTodo = (text) => {
  return Api.post("/todos", {
    text,
    done: false,
  });
};

export const deleteTodo = (id) => {
  return Api.delete(`/todos/${id}`);
};

export const updateTodo = (id, {text, done}) => {
  return Api.put(`/todos/${id}`, {
    text,
    done
  });
};


export default {
    getTodos,
    createTodo,
    deleteTodo,
    updateTodo,
    getTodoById
}
