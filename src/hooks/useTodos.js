import { useDispatch } from "react-redux";
import todosApi from "../apis/todos";
import { loadTodos } from "../components/TodoSlice";

const useTodos = () => {
  const dispatch = useDispatch();

  const getTodos = async () => {
    const { data } = await todosApi.getTodos();
    dispatch(loadTodos(data));
    return data;
  };

  const getTodoById = async (id) => {
    const { data } = await todosApi.getTodoById(id);
    return data;
  };

  const createTodo = async (text) => {
    await todosApi.createTodo(text);
    getTodos();
  };

  const deleteTodo = async (id) => {
    await todosApi.deleteTodo(id);
    getTodos();
  };

  const updateTodo = async (id, {text, done}) => {
    await todosApi.updateTodo(id, {text, done});
    getTodos();
  };

  return {
    getTodos,
    createTodo,
    deleteTodo,
    updateTodo,
    getTodoById
  };
};

export default useTodos;
