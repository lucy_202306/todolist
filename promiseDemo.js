const promise = new Promise((resolve, reject) => {
  const randomNumber = Math.random() * 100;
  if (randomNumber < 50) {
    resolve(`promise成功返回 ${randomNumber}`);
  } else {
    reject(`promise失败返回 ${randomNumber}`);
  }
});

promise
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.error(error);
  });
