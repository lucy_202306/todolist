# day14 - ORID
## O
1. code review
2. Use concept map to summarize the knowledge learned from the front-end
3. Use springboot and mysql to develop the back-end interface of todolist and interact with the front-end
4. The group gave the elevator speech and the MVP speech
## R
fruitful and relaxed
## I
1. When the backend was running at noon, the project kept reporting errors. At first, I thought it was a database creation problem. Later, with the help of Daniel, I finally found out that it was a port occupation problem and solved it.
2. Through group presentations, we learned about elevator presentations, MVP, user stories, and user journeys
## D
1. Review the previous exercises to familiarize myself with springBoot.
2. Although front-end training is almost over, I still want to enrich front-end knowledge in private

